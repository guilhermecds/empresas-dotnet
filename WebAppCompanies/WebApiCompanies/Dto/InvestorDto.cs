﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiCompanies.Dto
{
    public class InvestorDto
    {
        public InvestorDadosDto investor { get; set; }
        public string enterprise { get; set; }
        public bool success { get; set; }
        public string token { get; set; }
    }
}

