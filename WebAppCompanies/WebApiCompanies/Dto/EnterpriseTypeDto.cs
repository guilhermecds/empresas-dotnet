﻿namespace WebApiCompanies.Dto
{
    public class EnterpriseTypeDto
    {
        public int id { get; set; }
        public string enterprise_type_name { get; set; }
    }
}