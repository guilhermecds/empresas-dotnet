﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiCompanies.Dto
{
    public class PortfolioDto
    {
        public int enterprises_number { get; set; }
        public List<EnterprisesDto> enterprises { get; set; }
    }
}

