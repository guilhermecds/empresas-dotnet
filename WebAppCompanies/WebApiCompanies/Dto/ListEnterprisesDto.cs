﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiCompanies.Dto
{
    public class ListEnterprisesDto
    {
        public List<EnterprisesDto> enterprises { get; set; }
    }
}