﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiCompanies.Models
{
    public class InvestorEnterprise
    {
        public int id { get; set; }
        public int identerprise { get; set; }
        public string enterprise_name { get; set; }
        public string description { get; set; }
        public string email_enterprise { get; set; }
        public string facebook { get; set; }
        public string twitter { get; set; }
        public string linkedin { get; set; }
        public bool own_enterprise { get; set; }
        public string phone { get; set; }
        public int value { get; set; }
        public int shares { get; set; }
        public decimal share_price { get; set; }
        public int own_shares { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public int fkenterprise_type { get; set; }
        public string enterprise_type_name { get; set; }
        public string investor_name { get; set; }
        public string email { get; set; }
        public string city_investor { get; set; }
        public string country_investor { get; set; }
        public decimal balance { get; set; }
        public string photo { get; set; }
        public decimal portfolio_value { get; set; }
        public bool first_access { get; set; }
        public bool super_angel { get; set; }
    }
}