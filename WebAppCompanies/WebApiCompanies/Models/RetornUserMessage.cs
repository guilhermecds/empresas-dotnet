﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiCompanies.Models
{
    public class RetornUserMessage
    {
        public bool success { get; set; }
        public List<string> errors { get; set; }
    }
}