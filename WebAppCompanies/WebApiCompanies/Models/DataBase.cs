﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApiCompanies.Models
{
    public class DataBase
    {
        SqlConnection con = new SqlConnection();
        public SqlConnection abreconexao()
        {
            //string scon = System.Configuration.ConfigurationSettings.AppSettings["connect"];
            string scon = ConfigurationManager.ConnectionStrings["connect"].ConnectionString;

            con.ConnectionString = scon;
            con.Open();
            return con;
        }

        public DataTable consulta(string sql)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(sql, abreconexao());

            while (con.State != ConnectionState.Open)
            {

                abreconexao();
            }
            if (con.State == ConnectionState.Open)
            {
                da.Fill(dt);
                con.Close();
                con.Dispose();
            }
            return dt;
        }
    }
}