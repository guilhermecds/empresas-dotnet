﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace WebApiCompanies.Models
{
    public static class Utils
    {
        private static Dictionary<Type, IList<PropertyInfo>> typeDictionary = new Dictionary<Type, IList<PropertyInfo>>();

        public static List<T> ToList<T>(this DataTable table) where T : new()
        {          
            IList<PropertyInfo> properties = GetPropertiesForType<T>();
            IList<T> result = (
            from object row in table.Rows
            select CreateItemFromRow<T>((DataRow)row, properties)).ToList();
            return result.ToList();
        }

        public static IList<PropertyInfo> GetPropertiesForType<T>()
        {
            var type = typeof(T);
            if (!typeDictionary.ContainsKey(typeof(T)))
            {
                typeDictionary.Add(type, type.GetProperties().ToList());
            }
            return typeDictionary[type];
        }

        private static T CreateItemFromRow<T>(DataRow row, IList<PropertyInfo> properties) where T : new()
        {
            T item = new T();
            foreach (var property in properties)
            {             
                if (!DBNull.Value.Equals(row[property.Name]))
                    property.SetValue(item, row[property.Name], null);
            }
            return item;
        }
    }
}