﻿namespace WebApiCompanies.Models
{
    public class EnterpriseType
    {
        public int id { get; set; }
        public string enterprise_type_name { get; set; }
    }
}