﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace WebApiCompanies.Models
{
    public class ServiceModel
    {
        public DataTable GetInvestor(string email, string password)
        {
            string sql = @"select distinct 
                            enterprise.id as identerprise,
                            enterprise.enterprise_name,
                            enterprise.description,
                            enterprise.email_enterprise,
                            enterprise.facebook,
                            enterprise.twitter,
                            enterprise.linkedin,
                            enterprise.own_enterprise,
                            enterprise.phone,
                            enterprise.value,
                            enterprise.shares,
                            enterprise.share_price,
                            enterprise.own_shares,
                            enterprise.city,
                            enterprise.country,
                            enterprisetype.id as fkenterprise_type,
                            enterprisetype.enterprise_type_name,
                            investor.id,
                            investor.investor_name,
                            investor.email,
                            investor.city as city_investor,
                            investor.country as country_investor, 
                            investor.balance,
                            investor.photo,
                            investor.portfolio_value,
                            investor.first_access,
                            investor.super_angel
                           from  
                            enterprise, enterprisetype, invertorenterprises, investor
                           where enterprise.fkenterprise_type = enterprisetype.id 
                            and invertorenterprises.id_investor = investor.id 
                            and enterprise.id = invertorenterprises.id_enterprise
                            and investor.email = '" + email + "' and password =" + password;
            DataBase bd = new DataBase();
            return bd.consulta(sql);
        }

        public DataTable GetEnterprise()
        {
            string sql = @"select  distinct
                             a.id,
	                         email_enterprise,
                             facebook,
                             twitter,
                             linkedin,
                             phone,
                             own_enterprise,
                             enterprise_name,
                             photo,
                             description,
                             city,
                             country,
                             value,
                             share_price,
                             fkenterprise_type,
                             b.enterprise_type_name
                              from enterprise a INNER JOIN enterprisetype b ON a.fkenterprise_type = b.id";  
            DataBase bd = new DataBase();
            return bd.consulta(sql);
        }

        public DataTable GetEnterpriseId(string id)
        {            
            string sql = @"select  distinct
                             a.id,
	                         email_enterprise,
                             facebook,
                             twitter,
                             linkedin,
                             phone,
                             own_enterprise,
                             enterprise_name,
                             photo,
                             description,
                             city,
                             country,
                             value,
                             share_price,
                             fkenterprise_type,
                             b.enterprise_type_name
                              from enterprise a INNER JOIN enterprisetype b ON a.fkenterprise_type = b.id and a.id =" + id;
            DataBase bd = new DataBase();
            return bd.consulta(sql);
        }

        public DataTable GetEnterpriseTypesNames(string enterprise_types, string name)
        {
            string sql = @"select  distinct
                            a.id,
	                        email_enterprise,
                            facebook,
                            twitter,
                            linkedin,
                            phone,
                            own_enterprise,
                            enterprise_name,
                            photo,
                            description,
                            city,
                            country,
                            value,
                            share_price,
                            FkEnterprisesTypes,
                            b.enterprise_type_name
                            from Enterprises a INNER JOIN Enterprisestypes b ON a.FkEnterprisesTypes = b.id and enterprise_name like'%" + name + "%' and FkEnterprisesTypes = " + enterprise_types;
            DataBase bd = new DataBase();
            return bd.consulta(sql);
        }

    }
}