﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebApiCompanies.Models;

namespace WebApiCompanies.Controllers
{
    public class Token
    {
        private string UrlDefault
        {
            get
            {
                return ConfigurationManager.AppSettings["UrlDefault"].ToString();
            }
        }
        private readonly HttpClient client;
        public Token()
        {
            client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        public TokenAcesso GetCadastroToken(Autenticacao autenticacao)
        {
            using (var http = client)
            {
                var body = "grant_type=password&username=" + autenticacao.email + "&password=" + autenticacao.password;

                var content = new StringContent(body, Encoding.UTF8, "application/x-www-form-urlencoded");
                HttpResponseMessage response = http.PostAsync(UrlDefault+"/token", content).GetAwaiter().GetResult();


                var responseContent = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<TokenAcesso>(responseContent);
            }
        }
    }
}