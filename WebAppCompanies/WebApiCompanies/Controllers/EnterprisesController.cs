﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApiCompanies.Dto;
using WebApiCompanies.Models;

namespace WebApiCompanies.Controllers
{
    [RoutePrefix("api/v1")]
    public class EnterprisesController : ApiController
    {
        // GET: Usuarios
        [Authorize]
        [HttpGet]
        [ResponseType(typeof(ListEnterprisesDto))]
        [Route("enterprises")]
        public IHttpActionResult Enterprise()
        {
            try
            {
                DataTable dtresult = new ServiceModel().GetEnterprise();
                List<Enterprises> ItemListaOp = Utils.ToList<Enterprises>(dtresult);

                if (ItemListaOp.Count == 0)
                    return Content(HttpStatusCode.NotFound, new RetornMessage { status = 404, error = "Not Found" });

                ListEnterprisesDto retorno = new ListEnterprisesDto()
                {
                    enterprises = ListEnterpriseMapper(ItemListaOp)
                };
                return Ok(retorno);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, new RetornMessage { status = 500, error = "Internal Server Error" });
            }
        }              

        [HttpGet]
        [ResponseType(typeof(ListEnterprisesDto))]        
        [Route("enterprises/{id}")]
        public IHttpActionResult Enterprise(string id)
        {
            try
            {                
                DataTable dtresult = new ServiceModel().GetEnterpriseId(id);           
                List<Enterprises> ItemListaOp = Utils.ToList<Enterprises>(dtresult);

                if (ItemListaOp.Count == 0)
                    return Content(HttpStatusCode.NotFound, new RetornMessage { status = 404, error = "Not Found"});             


                ListEnterprisesDto retorno = new ListEnterprisesDto()
                {
                    enterprises = ListEnterpriseMapper(ItemListaOp)
                };

                return Ok(retorno);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, new RetornMessage { status = 500, error = "Internal Server Error" });
            }
        }

        [HttpGet]
        [ResponseType(typeof(ListEnterprisesDto))]        
        [Route("enterprises")]
        public IHttpActionResult Enterprise(string enterprise_types, string name)
        {
            try
            {                
                DataTable dtresult = new ServiceModel().GetEnterpriseTypesNames(enterprise_types, name);           
                List<Enterprises> ItemListaOp = Utils.ToList<Enterprises>(dtresult);

                if (ItemListaOp.Count == 0)
                    return Content(HttpStatusCode.NotFound, new RetornMessage { status = 404, error = "Not Found"});             


                ListEnterprisesDto retorno = new ListEnterprisesDto()
                {
                    enterprises = ListEnterpriseMapper(ItemListaOp)
                };

                return Ok(retorno);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.InternalServerError, new RetornMessage { status = 500, error = "Internal Server Error" });
            }
        }




        private List<EnterprisesDto> ListEnterpriseMapper(List<Enterprises> itemListaOp)
        {
            return itemListaOp?.Select(x => new EnterprisesDto
            {
                id = x.id,
                email_enterprise = x.email_enterprise,
                facebook = x.facebook,
                twitter = x.twitter,
                linkedin = x.linkedin,
                phone = x.phone,
                own_enterprise = x.own_enterprise,
                enterprise_name = x.enterprise_name,
                photo = x.phone,
                description = x.description,
                city = x.city,
                country = x.country,
                value = x.value,
                share_price = x.share_price,
                enterprise_type = itemListaOp.Where(m => m.fkenterprise_type == x.fkenterprise_type).Select(y => new EnterpriseTypeDto()
                {
                    id = y.fkenterprise_type,
                    enterprise_type_name = y.enterprise_type_name
                }).SingleOrDefault()
            }).ToList();
        }
                //enterprise_type = itemListaOp.Where(m => m.fkenterprise_type == x.fkenterprise_type).Select(y => new EnterpriseTypeDto()
                //{
                //    id = y.fkenterprise_type,
                //    enterprise_type_name = y.enterprise_type_name                 
                //}).ToList()


        //Listagem de Empresas
        //Detalhamento de Empresas
        //Filtro de Empresas por nome e tipo
    }
}