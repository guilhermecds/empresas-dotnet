﻿using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using WebApiCompanies.Dto;
using WebApiCompanies.Models;

namespace WebApiCompanies.Controllers
{
    [RoutePrefix("api/v1")]
    public class UserController : ApiController
    {
        [HttpPost]
        [ResponseType(typeof(List<InvestorDto>))]       
        [Route("users/auth/sign_in/")]
        public IHttpActionResult GetUser([FromBody]Autenticacao autenticacao)
        {       

            DataTable dtresultuser = new ServiceModel().GetInvestor(autenticacao.email, autenticacao.password);
            List<InvestorEnterprise> ItemListaInvest = Utils.ToList<InvestorEnterprise>(dtresultuser);
         
            if (ItemListaInvest.Count == 0)
            {
                List<string> error = new List<string>();
                error.Add("Invalid login credentials. Please try again");
                return Content(HttpStatusCode.NotFound, new RetornUserMessage { success = false, errors = error });
            }

            var token = new Token().GetCadastroToken(autenticacao);

            InvestorDto retorno = new InvestorDto()
            {
                investor = ListInvestorEnterpriseMapper(ItemListaInvest),
                enterprise = null,
                success = true,
                token = token.access_token
            };


            return Ok(retorno);
        }

        private InvestorDadosDto ListInvestorEnterpriseMapper(List<InvestorEnterprise> itemListaInvest)
        {
            return new InvestorDadosDto()
            {
                id = itemListaInvest.Select(x => x.id).FirstOrDefault(),
                investor_name = itemListaInvest.Select(x => x.investor_name).FirstOrDefault(),
                email = itemListaInvest.Select(x => x.email).FirstOrDefault(),
                city = itemListaInvest.Select(x => x.city).FirstOrDefault(),
                country = itemListaInvest.Select(x => x.country).FirstOrDefault(),
                balance = itemListaInvest.Select(x => x.balance).FirstOrDefault(),
                photo = itemListaInvest.Select(x => x.photo).FirstOrDefault(),
                portfolio_value = itemListaInvest.Select(x => x.portfolio_value).FirstOrDefault(),
                first_access = itemListaInvest.Select(x => x.first_access).FirstOrDefault(),
                super_angel = itemListaInvest.Select(x => x.super_angel).FirstOrDefault(),
                portfolio = itemListaInvest.Select(y => new PortfolioDto()
                {
                    enterprises_number = itemListaInvest.Count,
                    enterprises = MapperEnterprise(itemListaInvest)
                }).FirstOrDefault()
            };

        
        }

        private List<EnterprisesDto> MapperEnterprise(List<InvestorEnterprise> itemListaInvest)
        {
            return itemListaInvest.Select(x => new EnterprisesDto
            {
                id = x.identerprise,
                email_enterprise = x.email_enterprise,
                facebook = x.facebook,
                twitter = x.twitter,
                linkedin = x.linkedin,
                phone = x.phone,
                own_enterprise = x.own_enterprise,
                enterprise_name = x.enterprise_name,
                photo = x.phone,
                description = x.description,
                city = x.city,
                country = x.country,
                value = x.value,
                share_price = x.share_price,
                enterprise_type = itemListaInvest.Where(m => m.fkenterprise_type == x.fkenterprise_type).Select(y => new EnterpriseTypeDto()
                {
                    id = y.fkenterprise_type,
                    enterprise_type_name = y.enterprise_type_name
                }).SingleOrDefault()
            }).ToList();

        }
    }
}