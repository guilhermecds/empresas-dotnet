CREATE DATABASE Ioasys

USE [Ioasys]
GO
/****** Object:  Table [dbo].[enterprise]    Script Date: 23/08/2019 22:30:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[enterprise](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[enterprise_name] [varchar](255) NULL,
	[description] [varchar](max) NULL,
	[email_enterprise] [varchar](255) NULL,
	[facebook] [varchar](255) NULL,
	[twitter] [varchar](255) NULL,
	[linkedin] [varchar](255) NULL,
	[phone] [varchar](255) NULL,
	[own_enterprise] [bit] NULL,
	[photo] [varchar](255) NULL,
	[value] [int] NULL,
	[shares] [int] NULL,
	[share_price] [money] NULL,
	[own_shares] [int] NULL,
	[city] [varchar](255) NULL,
	[country] [varchar](255) NULL,
	[fkenterprise_type] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[enterprisetype]    Script Date: 23/08/2019 22:30:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[enterprisetype](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[enterprise_type_name] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[invertorenterprises]    Script Date: 23/08/2019 22:30:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[invertorenterprises](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_investor] [int] NULL,
	[id_enterprise] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[investor]    Script Date: 23/08/2019 22:30:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[investor](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[investor_name] [varchar](255) NULL,
	[email] [varchar](255) NULL,
	[city] [varchar](255) NULL,
	[country] [varchar](255) NULL,
	[balance] [money] NULL,
	[photo] [varchar](255) NULL,
	[portfolio_value] [money] NULL,
	[first_access] [bit] NULL,
	[super_angel] [bit] NULL,
	[password] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[enterprise]  WITH CHECK ADD FOREIGN KEY([fkenterprise_type])
REFERENCES [dbo].[enterprisetype] ([id])
GO
ALTER TABLE [dbo].[invertorenterprises]  WITH CHECK ADD FOREIGN KEY([id_enterprise])
REFERENCES [dbo].[enterprise] ([id])
GO
ALTER TABLE [dbo].[invertorenterprises]  WITH CHECK ADD FOREIGN KEY([id_investor])
REFERENCES [dbo].[investor] ([id])
GO
