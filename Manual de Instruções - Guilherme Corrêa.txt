******************** Ol�, bem vindo! ******************

***Configurar Banco de Dados***
Para desfrutar da nossa WebApi, voc� vai precisar compilar o Script.sql no banco de dados no SQL Server. (Preferencialmente SQL Server 2017);
Ap�s isso, voc� poder� popular os dados no banco atrav�s do arquivo dadosdb.txt

***Configurar WebApi***
Configurar o banco de dados no WebConfig  --<connectionStrings> name= connect 
Configurar endere�o da Api no WebConfig --<appSettings> Key = UrlDefault

***Utiliza��o da WebApi***
Feito isso � s� iniciar a api e fazer a autentica��o, obter um token e nas pr�ximas requisi��es, adicionar no Header 
Key: Authorization,  value:  bearer token_gerado para prosseguir com os servi�os.

Obs:. Foi adicionado um campo "token" no retorno do Endpoint api/v1/users/auth/sign_in para facilitar a obten��o do mesmo.

***EndPoints dispon�veis***
api/v1/users/auth/sign_in
api/v1/enterprises?enterprise_types=7&name=Tin
api/v1/enterprises/1
api/v1/enterprises/

----------Configura��es------------
Microsoft Visual Studio Community 2017
Version 15.9.15
.Net Framework 4.6.1

Microsoft SQL Server Express Edition (64-bit) 2017
Vers�o Produto 14.0.1000 RTM
